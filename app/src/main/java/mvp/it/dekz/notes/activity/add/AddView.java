package mvp.it.dekz.notes.activity.add;

import mvp.it.dekz.notes.base.View;

public interface AddView extends View {
    void onNoteSaved();
}
