package mvp.it.dekz.notes.activity.login;
import mvp.it.dekz.notes.base.View;

public interface LoginView extends View {
    void onTokenGet();
}
