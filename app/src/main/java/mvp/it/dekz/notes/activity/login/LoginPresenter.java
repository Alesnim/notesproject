package mvp.it.dekz.notes.activity.login;

import android.content.SharedPreferences;

import java.util.UUID;

import mvp.it.dekz.notes.base.Presenter;

public class LoginPresenter implements Presenter<LoginView> {
    private LoginView loginView;

    public void saveToken(String token, SharedPreferences prefs){
        prefs.edit().putString("access-token", token).apply();
    }


    private String generateID(){
        return UUID.randomUUID().toString();
    }

    @Override
    public void onAttach(LoginView View) {
        this.loginView = View;
    }

    @Override
    public void onDetach() {
        loginView = null;
    }
}
