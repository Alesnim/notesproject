package mvp.it.dekz.notes.tasks;

import android.os.AsyncTask;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;

public class UserAccountTask extends AsyncTask<Void, Void, FullAccount> {

    private DbxClientV2 dbxClientV2;
    private TaskDelegate delegate;
    private Exception error;


    public interface TaskDelegate {
        void onAccountReceived(FullAccount account);
        void onError(Exception error);
    }

    public UserAccountTask(DbxClientV2 dbxClient, TaskDelegate delegate){
        this.dbxClientV2 =dbxClient;
        this.delegate = delegate;
    }

    @Override
    protected FullAccount doInBackground(Void... params) {
        try {
            return dbxClientV2.users().getCurrentAccount();
        }
        catch (DbxException e){
            e.printStackTrace();
            error = e;
        }
        return null;
    }


    @Override
    protected void onPostExecute(FullAccount fullAccount) {
        super.onPostExecute(fullAccount);

        if (fullAccount !=null && error == null){
            delegate.onAccountReceived(fullAccount);
        }
        else {
            delegate.onError(error);
        }
    }
}
