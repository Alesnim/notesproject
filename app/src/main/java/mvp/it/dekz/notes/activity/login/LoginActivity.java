package mvp.it.dekz.notes.activity.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.users.FullAccount;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import mvp.it.dekz.notes.R;
import mvp.it.dekz.notes.tasks.DownloadTask;
import mvp.it.dekz.notes.tasks.UploadTask;
import mvp.it.dekz.notes.tasks.UserAccountTask;
import mvp.it.dekz.notes.utils.DropClient;

public class LoginActivity extends AppCompatActivity implements LoginView{
    @BindView(R.id.account_name) TextView accountName;
    @BindView(R.id.btn_backup) Button backup;
    @BindView(R.id.btn_restore) Button restore;
    @BindView(R.id.btn_login) Button login;
    LoginPresenter loginPresenter;
    String token;
    private final String TAG = "LoginDrop";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter();


        if (!hasToken()){
            Auth.startOAuth2Authentication(LoginActivity.this, getString(R.string.APP_KEY));
        }
        else {
            token = retrieveAccessToken();
            getUserAccount();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasToken()){
                    token = retrieveAccessToken();
                    getUserAccount();
                }
                else{
                    getAccessToken();
                    getUserAccount();
                }
            }
        });

        backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload();
            }
        });

        restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                download();
            }
        });
    }

    private void download() {
        if (!hasToken()){
            return;
        }
        Realm realm = Realm.getDefaultInstance();
        String realmPath = realm.getPath();
        File realmFile = new File(realmPath);
        realmFile.delete();
        realmFile = new File(realmPath);
        new DownloadTask(DropClient.getClient(token), realmFile, getApplicationContext()).execute();

    }

    private void upload() {
        if (!hasToken()) {
            return;
        }
        Realm realm = Realm.getDefaultInstance();
        File realmFile = new File(realm.getPath());
        new UploadTask(DropClient.getClient(token), realmFile, getApplicationContext()).execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAccessToken();
        onAttachView();

    }

    private String retrieveAccessToken() {
        SharedPreferences prefs = getSharedPreferences("mvp.it.dekz.notes", Context.MODE_PRIVATE);
        String accessToken = prefs.getString("access-token", null);
        if (accessToken == null) {
            Log.d("AccessToken Status", "No token found");
            Auth.startOAuth2Authentication(LoginActivity.this, getString(R.string.APP_KEY));
            getAccessToken();
            return token;
        } else {
            //accessToken already exists
            Log.d("AccessToken Status", "Token exists");
            return accessToken;
        }
    }
   protected void getUserAccount(){
        new UserAccountTask(DropClient.getClient(token), new UserAccountTask.TaskDelegate() {
            @Override
            public void onAccountReceived(FullAccount account) {
                updateInfo(account);
                Log.d(TAG, (String) account.getEmail());
            }

            @Override
            public void onError(Exception error) {
                Log.d(TAG, "Error get account " );
            }
        }).execute();
   }

    private void updateInfo(FullAccount account) {
        accountName.setText(account.getEmail());
    }

    private boolean hasToken() {
        SharedPreferences prefs = getSharedPreferences("mvp.it.dekz.notes", Context.MODE_PRIVATE);
        String accessToken = prefs.getString("access-token", null);
        return accessToken != null;
    }

    @Override
    public void onBackPressed() {
        onDetachView();
        LoginActivity.this.finish();
    }

    @Override
    protected void onDestroy(){
        onDetachView();
        super.onDestroy();
    }

    private void getAccessToken() {
        String token = Auth.getOAuth2Token();
        if (token != null){
            SharedPreferences preferences = getSharedPreferences("mvp.it.dekz.notes", Context.MODE_PRIVATE);
            loginPresenter.saveToken(token, preferences);
            this.token = token;
        }
    }



    @Override
    public void onTokenGet() {
        Toast.makeText(getApplicationContext(), "auth success", Toast.LENGTH_SHORT).show();
        LoginActivity.this.finish();
    }

    @Override
    public void onAttachView() {
        loginPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        loginPresenter.onDetach();
    }
}
