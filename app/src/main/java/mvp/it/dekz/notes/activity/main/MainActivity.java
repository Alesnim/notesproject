package mvp.it.dekz.notes.activity.main;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.os.Environment;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import mvp.it.dekz.notes.R;
import mvp.it.dekz.notes.activity.add.AddActivity;
import mvp.it.dekz.notes.activity.login.LoginActivity;
import mvp.it.dekz.notes.activity.main.fragment.listnotes.ListNotesFragment;
import mvp.it.dekz.notes.model.Note;

public class MainActivity extends AppCompatActivity implements MainView {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    MainPresenter mainPresenter;
    @BindView(R.id.fabAdd)FloatingActionButton add;
    @BindView(R.id.tbOption)TextView options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(options.getContext(),options);
                menu.inflate(R.menu.menu_toolbar);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.backup:
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                break;
                        }
                    return false;
                    }
                });
                menu.show();
            }
        });

        mainPresenter = new MainPresenter();
        onAttachView();
        checkStoragePermissions(this);

    }

    @Override
    public void onAttachView() {
        mainPresenter.onAttach(this);
        //get data from realm
        mainPresenter.getNotesList();

    }

    @Override
    public void onDetachView() {
        mainPresenter.onDetach();
    }

    @Override
    protected void onDestroy(){
        onDetachView();
        super.onDestroy();
    }

    @Override
    public void onShowFragment() {
        Fragment fragment = ListNotesFragment.newInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction.replace(R.id.frame_main, fragment);
        fragmentTransaction.commit();
        
    }
    private void checkStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}
